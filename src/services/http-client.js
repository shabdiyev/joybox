import axios from "axios";
import { parseCookies } from "nookies";
import { QueryClient } from "react-query";

export const request = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL,
  headers: {
    Authorization:
      "Basic Y2tfMzk4MjQxZjU4YjNiOWI1YjMyMTNmNmUzNjQyNWUwODk1NzViNGNlNDpjc181NzZkZjVhODhlNGJjYWY2OTI2OTY5OTIzMmQ4ZmVhZGEzNWZmZTNk",
  },
});

const errorHandler = (error) => {
  console.log("error ===>", error);

  return Promise.reject(error.response);
};

request.interceptors.response.use((response) => response.data, errorHandler);

export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      retry: false,
    },
  },
});
