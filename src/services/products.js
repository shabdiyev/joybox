import { request } from "./http-client";
const getProducts = (params) => request.get("/products", { params });
