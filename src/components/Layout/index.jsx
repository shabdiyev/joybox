import Footer from "@/components/UI/Footer";
import Header from "@/components/UI/Header";

export default function Layout({ children }) {
  return (
    <div className="App">
      <Header />
      <main>{children}</main>
      <Footer />
    </div>
  );
}
