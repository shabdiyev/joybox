import useTranslation from "next-translate/useTranslation";
import { useRouter } from "next/router";
import Link from "next/link";

export default function Header() {
  const router = useRouter();
  const { t } = useTranslation("common");

  return (
    <header className="bg-gray-900 text-white py-4">
      <div className="container mx-auto flex justify-between items-center">
        <Link href="/">
          <a>
            <div className="text-xl font-bold">Joybox</div>
          </a>
        </Link>
      </div>
    </header>
  );
}
