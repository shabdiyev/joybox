import React from "react";
import Category from "./Category";
import { Container } from "@mui/material";
import { Typography } from "@mui/material";

export default function Categories({ categories }) {
  return (
    <Container className="my-6">
      <Typography className="mt-3" variant="h4" component="h4">
        Категории
      </Typography>
      <div className="flex flex-wrap justify-center">
        {categories.map((category, index) => (
          <Category
            key={index}
            imageUrl={category?.image?.src}
            name={category.name}
            id={category.id}
          />
        ))}
      </div>
    </Container>
  );
}
