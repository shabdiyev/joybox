export default function Footer() {
  return (
    <footer className="bg-gray-900 text-white py-4 ">
      <div className="container mx-auto flex justify-between items-center">
        <div className="text-sm">&copy; 2024 Your Company</div>
        <nav className="space-x-4">
          <a href="#" className="hover:text-gray-500">
            Home
          </a>
          <a href="#" className="hover:text-gray-500">
            About
          </a>
          <a href="#" className="hover:text-gray-500">
            Services
          </a>
          <a href="#" className="hover:text-gray-500">
            Contact
          </a>
        </nav>
      </div>
    </footer>
  );
}
