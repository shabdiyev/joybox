import Link from "next/link";

const ProductItem = ({ name, id }) => {
  return (
    <div className="w-[220px]  rounded overflow-hidden shadow-[rgba(0,_0,_0,_0.16)_0px_1px_4px] m-4">
      <Link
        href={`/product/${id}` || ""}
        className="block relative cursor-pointer "
      >
        <a>
          <div className="px-6 py-4 text-center">
            <div className="font-bold text-xl mb-2">{name}</div>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default ProductItem;
